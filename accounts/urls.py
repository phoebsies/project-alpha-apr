from django.urls import path
from accounts.views import login_view, logout_view, signup


urlpatterns = [
    path("accounts/login/", login_view, name="login"),
    path("accounts/logout/", logout_view, name="logout"),
    path("accounts/signup/", signup, name="signup"),
]
