from django.contrib import admin
from django.urls import path, include
from projects.views import list_projects, show_project, create_project
from accounts.views import login_view, logout_view, signup
from tasks.views import create_task, show_my_tasks


urlpatterns = [
    path("admin/", admin.site.urls),
    path("", include("projects.urls")),
    path("", list_projects, name="home"),
    path("accounts/login/", include("django.contrib.auth.urls")),
    path("accounts/login/", login_view, name="login"),
    path("accounts/logout/", logout_view, name="logout"),
    path("accounts/signup/", signup, name="signup"),
    path("projects/show_project/<int:id>/", show_project, name="show_project"),
    path("projects/create/", create_project, name="create_project"),
    path("tasks/create/", create_task, name="create_task"),
    path("tasks/mine/", show_my_tasks, name="show_my_tasks"),
]
